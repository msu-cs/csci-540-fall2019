# Streaming Algorithms

Starting 10/07 we moved away from non-relational models in databases and began
studying methods for how to process large data sets. In particular, we covered
streaming hashing, streaming algorithms, and probabilistic data structures.  In
this assignment, we will do some implementation and experimentation.

For your implementations, you may use whatever languages you like.  In addition,
you may (and I strongly suggest) use a library that provides non-cryptographic
hash functions.  You may used additional libraries and frameworks (such as Flume
or Flink), but you may NOT use their algorithm implementations.

Many of the algorithms that we have discussed have multiple parameters, for
which we have some rules of thumb.  But, once you have an implementation, you
have the opportunity to get a better sense of how changing the parameters
affect the results.

## Hash Tables

We studied two open addressing schemes for building hash tables, _linear
probing_ and _cuckoo hashing_.  Implement one of the two schemes.

Each of the schemes has a few parameters.  Select one of the parameters that you
are interested in investigating.  Propose and implement an experiment that lets
you investigate how changing the parameter (explanatory variable) affects the
_quality_ of scheme (response variable).  Write up your experiment, the results,
and analysis. Your writeup should have at least one paragraph explaining the
experiment, at least one plot to visualize the experimental data, and at least
one  paragraph analyzing the results of the experiment/discussing the plot.

**What To Submit**  In a folder `01-hashing` include the code for your hash
table and experiment

## Streaming Algorithms

We have studied three problems in which streaming algorithms are of use: _set
membership_, _cardinality_, and _frequency_.  Pick two of the three problems and
implement a one pass streaming algorithm (excluding The Majority
Algorithm) that solves the problem over very large data sets.  Your
algorithm should be one of the algorithms that we have studied in class.  (If you
are interested in a different algorithm that we didn't cover, please, ask and we
can decide if it is an appropriate substitute.)

As with hashing, for each algorithm you implemented, select one of the
parameters that you are interested in investigating.  Propose and implement an
experiment that lets you investigate how changing the parameter (explanatory
variable) affects the _quality_ of scheme (response variable).  Write up your
experiment, the results, and analysis. Your writeup should have at least one
paragraph explaining the experiment, at least one plot to visualize the
experimental data, and at least one  paragraph analyzing the results of the
experiment/discussing the plot.

**Extra Credit (up to +3/10 extra points)** address all three problems

**What To Submit**  In a folder with appropriate name (`02-set-membership`,
`03-cardinality`, `04-frequency`) include the code for your implementation.
Do not include the folder for the problem that you did not investigate (if any).

## Submission

Aggregate all of your writeups into a file called `experiments.pdf` in the top
level of the submission.

An example submission structure in which I did NOT investigate set membership is:

    lab06
    +- experiments.pdf
    +- 01-hashing
       +- hashtable.py
       +- experiment.py
       +- analysis.ipynb
    +- 03-cardinality
       +- cardinalty.py
       +- experiment.py
       +- analysis.ipynb
    +- 04-frequency
       +- frequency.py
       +- experiment.py
       +- analysis.ipynb

Note that you don't need to do the analysis in notebooks (the `.ipynb` files).
But, I like to so that I can more easily re-run an analysis.
