-- drop  if they exist
drop table if exists users cascade;
drop table if exists compensations cascade;

-- create tables
CREATE TABLE users (
    id serial primary key,
    name text NOT NULL,
    created_at timestamp default now() NOT NULL,
    updated_at timestamp default now() NOT NULL,
    email text DEFAULT ''::text NOT NULL
);


CREATE TABLE compensations (
    id serial primary key,
    name text,
    started_at timestamp NOT NULL,
    ended_at timestamp NOT NULL,
    rate_cents integer DEFAULT 0 NOT NULL,
    user_id integer
);

ALTER TABLE ONLY compensations
    ADD CONSTRAINT compensation_user_id FOREIGN KEY (user_id) REFERENCES users(id);

-- populate with some seed data
insert into users (name, created_at, updated_at, email) values
    ('Dave M', '1981-04-30', '1981-04-30', 'david.millman@montana.edu'),
    ('Bob T', '2017-09-05', '2017-09-05', 'bob@rtm.com');

insert into compensations (name, started_at, ended_at, rate_cents, user_id) values
    ('Math Party', '2017-09-05 12:00', '2017-09-05 02:00', 500,
        (select id from users where name = 'Dave M')
    ),
    ('Physics Party', '2017-09-05 11:00', '2017-09-05 04:00', 350,
        (select id from users where name = 'Dave M')
    ),
    ('GSoC''s Ice Cream social', '2017-09-02 10:00', '2017-09-03 23:59', 225, null);


-- PERFORM SOME QUERIES ON THE DATA
-- find min start time and max end time for a compensation





-- find the average rate for an event with the substring party





-- Write a query that, gives the users name and the number of compensations
-- that belong to them.  For this data set the
--   name  | count
-- --------+-------
--  Bob T  |     1
--  Dave M |     2
-- (2 rows)




-- user a window function to produce tuples that have the compensation name, the
-- users name, the compensations name and the number of compensations that
-- belong to the user.  E.g. For this dataset:
--   comp_name              |  users_name  | count
-- -------------------------+--------------+-------
--  Math Party              | Dave M       |     2
--  Physics Party           | Dave M       |     2
--  GSoC's Ice Cream social |              |     1
-- (3 rows)




-- create a trigger that updates the user updated at
-- whenever the user is updated


