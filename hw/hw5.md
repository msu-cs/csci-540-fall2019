# Polyglot persistence

In this assignment, we will build a system that uses at least two databases to
solve a problem in which you are interested. It intentionally vague to give you
some freedom to dive further into the different databases that we have explored
so far and apply the new ideas to your preferred subfield (e.g. data science,
systems, etc.).  You can discuss projects with others, but you may NOT work in
teams.

You will define your problem and solution but your system must have the
following.

1. Use at least two systems for data storage one of them must be the
   authoritative data source.

2. For the non-authoritative data source, you must be able to rebuild from the
   authoritative source.

Some options could be a twitter clone supporting a live subscription feed, a
data ingestion system that provides some some streaming queries (such as
windowing or aggregation), or a tool for analyzing a dynamic network.

You do not need to worry about a user interface to your system, but you will
likely need to design a few data models and link the systems together with a
higher level language.  You may choose whatever language you like.

## Submission

We will meet for approximately 15 min to discuss your project.  You should
prepare a 5 min presentation (slides are not needed, but they may be helpful).
The presentation should state the problem that you are solving and how you are
trying to solve it.  In particular, you must describe the role of each database
in your system and how you rebuild the non-authoritative source.  You should
prepare a 2-3 min demo of your system, and the remaining time will be question
and answer.  **Scheduling a time to meet is your responsibility.**.  Other than my
office hours, I have availability Mon after class and after seminar and Tue
afternoon.  Sometimes I am available Wed and Fri afternoons.
