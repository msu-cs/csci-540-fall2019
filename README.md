# CSCI 540: Advanced Database Systems

**NOTE: This is a live document and is subject to change throughout the
semester.**

Data is everywhere and often a database is a convenient way to store and process
it.  But is a relational database always the best way?  In this class we will
explore several advanced database models, computational paradigms
for processing large data sets, and searching (indexing) techniques.
Database models include spatial, key-value,
columnar, document, and graph; Computational paradigms for large data sets
include MapReduce and Streaming;  Searching techniques include approx-NN, LSH,
and inverted indices.

## Meeting Times

Mon, Wed, Fri 09:00-09:50, 332 Reid Hall

## Instructor

David L. Millman, Ph.D.

**Email**: david.millman@montana.edu

**Office hours**: Mon 15:00 - 15:50, Thurs 13:00-13:50, or by appointment

**Office**: Barnard Hall 359

**Github**: [dlm](https://github.com/dlm)

**Bitbucket**: [david_millman](https://bitbucket.org/david_millman/)


## Learning Outcomes

After successfully completing this course, students will be able to:

* Identify and Explain why a database or collections of databases is appropriate for a task
* Build a system using polyglot persistence
* Design and implement algorithms for searching and processing massive data sets


## Textbook

No required text book but optional are highly recommended

Optional and highly recommended:

* [Database System
  Concepts](https://www.amazon.com/Database-System-Concepts-Abraham-Silberschatz-ebook/dp/B07PPHYQGV)
  by Abraham Silberschatz, Henry F. Korth and S. Sudarshan
* [Seven Databases in Seven Weeks: A Guide to Modern Databases and the NoSQL
  Movement](https://www.amazon.com/Seven-Databases-Weeks-Modern-Movement/dp/1680502530)
  by Eric Redmond and Jim R. Wilson (7DB in reading below) (DO NOT USE 1st
  edition)
* [Probabilistic Data Structures and Algorithms for Big Data
  Applications](https://www.amazon.com/Probabilistic-Data-Structures-Algorithms-Applications/dp/3748190484)
  by Andrii Gakhov (PDS in reading below)
* [Lecture Notes from Modern Algorithmic
  Toolbox](http://timroughgarden.org/notes.html) by Tim Roughgarden and Greg
  Valiant (MAT below)
* [Mining of Massive Datasets](http://i.stanford.edu/~ullman/mmds/book0n.pdf) by
  Jure Leskovec, Anand Rajaraman, and Jeffrey D. Ullman (MMD below)
* SE-Radio:
    - [Advanced Postgres](https://www.se-radio.net/2019/04/se-radio-episode-362-simon-riggs-on-advanced-features-of-postgresql/)
    - [Query Planning](https://www.se-radio.net/2018/06/se-radio-episode-328-bruce-momjian-on-the-postgres-query-planner/)

Others will be added as relevant.

### Prerequisites

* **CSCI 440-- Database Systems**: DBMS architecture; major database models;
  relational algebra fundamentals; SQL query language; index file structures,
  data modeling and management, entity relationship diagrams.

* Comfort with a Unix based operating system.

* Willingness to get your hands dirty installing and working with multiple

## Class schedule

The lecture schedule is subject to change throughout the semester, but here is
the current plan. Assignments and due dates will be updated as they're assigned
in class.

### Aug

| Date  | Description                                  | Quiz                                                                   | Assigned                           | Due                                   | Recommended Reading                        |
|-------|----------------------------------------------|------------------------------------------------------------------------|------------------------------------|---------------------------------------|--------------------------------------------|
| 08/26 | [Intro](./notes/2019-08-26.pdf)              |                                                                        |                                    |                                       |                                            |
| 08/28 | [Env setup](./env/README.md)                 |                                                                        |                                    |                                       |                                            |
| 08/30 | [Relational](./notes/2019-08-30.sql)         | [Quiz 1](./quiz/2019-08-30.pdf) [(Solution)](./quiz/2019-08-30.sql)    | [Homework 0](./hw/hw0.pdf)         |                                       | 7DB-Relational Day 1                       |
|       |                                              |                                                                        |                                    |                                       |                                            |

### Sept

| Date  | Description                                  | Quiz                            | Assigned                           | Due                                   | Recommended Reading                        |
|-------|----------------------------------------------|---------------------------------|------------------------------------|---------------------------------------|-----------------------|
| 09/02 | NO CLASS (LABOR DAY)                         |                                 |                                    |                                       |                       |
| 09/04 | [Relational](./notes/2019-09-04.md)          |                                 |                                    |                                       | 7DB-Relational Day 2  |
| 09/06 | [Relational](./notes/2019-09-06.md)          |                                 |                                    | [Homework 0](./hw/hw0.pdf)            | 7DB-Relational Day 3  |
|       |                                              |                                 |                                    |                                       |                       |
| 09/09 | [Relational](./notes/2019-09-09.md)          |                                 | [Homework 1](./hw/hw1.pdf)         |                                       | 7DB-Relational Day 2  |
| 09/11 | [Column](./notes/2019-09-11.md)              |                                 |                                    |                                       | 7DB-Hbase Day 1       |
| 09/13 | [Column](./notes/2019-09-13.md)              |                                 | [Homework 2](./hw/hw2.pdf)         | [Homework 1](./hw/hw1.pdf)            | 7DB-Hbase Day 2       |
|       |                                              |                                 |                                    |                                       |                       |
| 09/16 | [Document](./notes/2019-09-16.md)            |                                 |                                    |                                       | 7DB-Mongo Day 1       |
| 09/18 | [Document](./notes/2019-09-18.md)            |                                 |                                    |                                       | 7DB-Mongo Day 2       |
| 09/20 | [Document](./quiz/2019-09-20.md)             | [Quiz 2](./quiz/2019-09-20.md)  | [Homework 3](./hw/hw3.md)          | [Homework 2](./hw/hw2.pdf)            | 7DB-Mongo Day 2       |
|       |                                              |                                 |                                    |                                       |                       |
| 09/23 | [Graph](./notes/2019-09-23.md)               |                                 |                                    |                                       | 7DB-Neo4j Day 1       |
| 09/25 | [Graph](./notes/2019-09-25.md)               | [Quiz 3](./quiz/2019-09-25.md)  |                                    |                                       | 7DB-Neo4j Day 2       |
| 09/27 | [Graph](./notes/2019-09-27.md)               |                                 | [Homework 4](./hw/hw4.md)          | [Homework 3](./hw/hw3.md)             | 7DB-Neo4j Day 2       |
|       |                                              |                                 |                                    |                                       |                       |
| 09/30 | [Redis](./notes/2019-09-30.md)               |                                 |                                    |                                       | 7DB-Redis Day 1       |

### Oct

| Date  | Description                                                                       | Quiz                            | Assigned                           | Due                              | Recommended Reading   |
|-------|-----------------------------------------------------------------------------------|---------------------------------|------------------------------------|----------------------------------|-----------------------|
| 10/02 | [Redis](./notes/2019-10-02.md)                                                    |                                 |                                    |                                  | 7DB-Redis Day 2       |
| 10/04 | [Redis](./hw/hw5.md)                                                              |                                 | [Homework 5](./hw/hw5.md)          | [Homework 4](./hw/hw4.md)        | 7DB-Redis Day 3       |
|       |                                                                                   |                                 |                                    |                                  |                       |
| 10/07 | [Hashing](./notes/2019-10-07.pdf)                                                 | [Quiz 4](./quiz/2019-10-07.md)  |                                    |                                  | PDS-Ch 1              |
| 10/09 | [Hashing](./notes/2019-10-09.pdf)                                                 |                                 |                                    |                                  | PDS-Ch 2              |
| 10/11 | [Set Membership](./notes/2019-10-11.pdf)                                          |                                 |                                    | [Homework 5](./hw/hw5.md)        | PDS-Ch 2              |
|       |                                                                                   |                                 |                                    |                                  |                       |
| 10/14 | [Cardinality](./notes/2019-10-14.pdf)                                             |                                 |                                    |                                  | PDS-Ch 3              |
| 10/16 | NO CLASS (DAVE SICK)                                                              |                                 |                                    |                                  |                       |
| 10/18 | NO CLASS (DAVE SICK)                                                              |                                 |                                    |                                  |                       |
|       |                                                                                   |                                 |                                    |                                  |                       |
| 10/21 | [Cardinality](./notes/2019-10-21.pdf)                                             |                                 | [Presentation](./hw/present.md)    |                                  | PDS-Ch 3              |
| 10/23 | [Cardinality](./notes/2019-10-23.pdf)                                             | [Quiz 5](./quiz/2019-10-23.md)  |                                    |                                  | PDS-Ch 3              |
| 10/25 | NO CLASS - [Frequency](./notes/2019-10-25.pdf) - [Video](http://bit.ly/2pSG5cP)   | [Quiz 6](./quiz/2019-10-25.md)  | [Homework 6](./hw/hw6.md)          | [Presentation](./hw/present.md)  | PDS-Ch 4              |
|       |                                                                                   |                                 |                                    |                                  |                       |
| 10/28 | [Frequency](./notes/2019-10-28.pdf)                                               |                                 |                                    |                                  | PDS-Ch 4              |
| 10/30 | [Frequency](./notes/2019-10-30.pdf)                                               |                                 |                                    |                                  | PDS-Ch 4              |

### Nov

| Date  | Description                                                   | Quiz                                 | Assigned                           | Due                                   | Recommended Reading                                                                           |
|-------|---------------------------------------------------------------|---------------------------------|------------------------------------|---------------------------------------|-----------------------------------------------------------------------------------------------|
| 11/01 | [MapReduce](./notes/2019-11-01.pdf)                           | [Quiz 7](./quiz/2019-11-01.md)  | [Homework 7](./hw/hw7/)            | [Homework 6](./hw/hw6.md)             | MMD-Ch 2                                                                                      |
|       |                                                               |                                 |                                    |                                       |                                                                                               |
| 11/04 | [MapReduce](./notes/2019-11-04)                               | [Quiz 8](./quiz/2019-11-04.md)  | [Proj Proposal](./hw/proj.md)      |                                       | MMD-Ch 2                                                                                      |
| 11/06 | [Similarity](./notes/2019-11-06.pdf)                          |                                 |                                    |                                       | MMD-Ch 3 / PDS-Ch 6                                                                           |
| 11/08 | [Similarity](./notes/2019-11-08.pdf)                          |                                 |                                    | [Homework 7](./hw/hw7/)               | MMD-Ch 3 / PDS-Ch 6                                                                           |
|       |                                                               |                                 |                                    |                                       |                                                                                               |
| 11/11 | NO CLASS (VETERANS DAY)                                       |                                 |                                    |                                       |                                                                                               |
| 11/13 | [Similarity](./notes/2019-11-13.pdf)                          |                                 |                                    |                                       | MMD-CH 3 / PDS-CH 6                                                                           |
| 11/15 | [Realtime DBs (Saha, Rahman)](./notes/2019-11-15.pdf)         |                                 | Exam                               | [Proj Proposal](./hw/proj.md)         | [Setup](notes/2019-11-13) [Overview Of RealtimeDBs](https://users.soe.ucsc.edu/~sbrandt/courses/Winter00/290S/rtdb.pdf) |
|       |                                                               |                                 |                                    |                                       |                                                                                               |
| 11/18 | [DB Security (Kelly, Turksonmez)](./notes/2019-11-18.pdf)     |                                 | [Proj Discussion](./hw/proj.md)    |                                       | [Setup](notes/2019-11-18)                                                                     |
| 11/20 | [Rainbow Tables (Johnson)](./notes/2019-11-20.pdf)            |                                 |                                    |                                       | [password hashing & salt](https://www.youtube.com/watch?v=--tnZMuoK3E)                        |
| 11/22 | [Blockchain DBs (Nelson)](./notes/2019-11-22.pdf)             |                                 |                                    | [Proj Discussion](./hw/proj.md)       | [Subspace](https://github.com/subspace/paper#abstract) [BigchainDB](https://www.bigchaindb.com/developers/guide/key-concepts-of-bigchaindb) [Blockchains](https://youtu.be/SSo_EIwHSd4) |
|       |                                                               |                                 |                                    |                                       |                                                                                               |
| 11/25 | [Multi-Obj Query Plan (Harris, Zou)](./notes/2019-11-25.pdf)  |                                 | [Proj](./hw/proj.md)               |                                       |                                                                                               |
| 11/27 | NO CLASS (THANKSGIVING BREAK)                                 |                                 |                                    |                                       | [Multi-Obj PQO](https://cacm.acm.org/magazines/2017/10/221322-multi-objective-parametric-query-optimization/fulltext) |
| 11/29 | NO CLASS (THANKSGIVING BREAK)                                 |                                 |                                    |                                       |                                                                                               |

### Dec

| Date  | Description                                                                       | Assigned                           | Due                                          | Recommended Reading                                                           |
|-------|-----------------------------------------------------------------------------------|------------------------------------|----------------------------------------------|-------------------------------------------------------------------------------|
| 12/02 | [Community Detection (Gibbs, Hewitt)](./notes/2019-12-02.pdf)                     |                                    |                                              | [Graph Algos Ch 6](https://neo4j.com/graph-algorithms-book/)                  |
| 12/04 | [Streaming Clustering (Folkman, Whitman)](./notes/2019-12-04.pdf)                 |                                    |                                              | [clustering](https://www.youtube.com/watch?v=4b5d3muPQmA) MMD--7.6            |
| 12/06 | [CouchDB versioning & Conflict Resolution (Hoy, Watson)](./notes/2019-12-06.pdf)  |                                    |                                              | [about couch](https://www.youtube.com/watch?v=aOE90VAVOcU)                    |
|
|       |                                                                                   |                                    |                                              |                                                                               |
| 12/09 | (Finals week) 08:00-09:50                                                         |                                    | [Proj Writeup & Presentation](./hw/proj.md)  |                                                                               |

### Potential Upcoming Topics:

* Journalling/Write ahead logging
* Compression

## Evaluation

Your grade for this class will be determined by:

* 10% Quizzes (lowest quiz is dropped)
* 35% Homework (lowest homework is dropped)
* 25% Exam
* 10% [Group Presentation](./hw/present.md)
* 20% [Group Project](./hw/proj.md)

## Policies

### Attendance

Attendance in class with not be taken but students are responsible for all
material covered in class.  If you are not in class, you cannot receive credit
for quizzes. Attendance is strongly recommended.

### Assignments

There will be regular homework assignments (about every week or every other week
depending on the difficulty of the assignment) consisting
of written problems and coding exercises.  Homeworks will be
posted in the schedule.  If not specified, solutions should be submitted as a
PDF on Brightspace.
(The tool that I use for grading documents only works with PDFs, so any file
format other than PDF will receive a 0.)
Homework is due at 23:59 on the due date. Late homework will not be accepted.

You do NOT need to write up your solutions with LaTex, but I highly encourage
you to do so.  You can find some resources for getting started with latex (and
for making figures, and keeping all those files safe with git) in the [student
resources repo](https://github.com/compTAG/student-resources).

I encourage collaboration, see collaboration section for details.

### Discussion

Group discussions, questions, and announcements will take place on the
Brightspace message board.
is okay to send me a direct message or email if you have a question that you feel
is not appropriate to share with the class.  If, however, you send me an message
with a question for which the response would be useful to the rest of the class,
I will likely ask you to post publicly.

### Collaboration

Collaboration IS encouraged, however, all submitted individual work must be your
own and you must acknowledge your collaborators at the beginning of the
submission.

On any group project, every team member is expected to make a substantial
contribution. The distribution of the work, however, is up to the team.

A few specifics for the assignments.  You may:

* Work with anyone in the course.
* Share ideas with others in the course
* Help other teams debug their code or proofs.

You may NOT:

* Submit a proof or code that you did not write.
* Modify another's proof or code and claim it as your own.

Using resources in addition to the course materials is encouraged. But, be sure
to properly cite additional resources. Remember, it is NEVER acceptable to pass
others work off as your own.

Paraphrasing or quoting another's work without citing the source is a form of
academic misconduct. Even inadvertent or unintentional misuse or appropriation
of another's work (such as relying heavily on source material that is not
acknowledged) is considered plagiarism. If you have any questions about using
and citing sources, you are expected to ask for clarification. My rule of thumb
is if I am in doubt, I cite.

By participating in this class, you agree to abide by the [student code of
conduct](http://www.montana.edu/policy/student_conduct/).  Please review the
policy.

### Classroom Etiquette

Except for note taking and coding, please keep electronic devices off during
class, they can be distractions to other students. Disruptions to the class will
result in you being asked to leave the lecture and will negatively impact your
grade.

### Special needs information

If you have a documented disability for which you are or may be requesting an
accommodation(s), you are encouraged to contact me and Disabled
Student Services as soon as possible.
