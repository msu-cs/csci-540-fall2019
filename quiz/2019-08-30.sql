-- drop  if they exist
drop table if exists students cascade;
drop table if exists classes cascade;
drop table if exists enrollments cascade;

-- 1. Write a SQL statement that creates a table for storing student information.
-- You can decide on what information is stored about the student.
create table students (
    id serial primary key,
    created_at timestamp default now() NOT NULL,
    updated_at timestamp default now() NOT NULL,
    name text NOT NULL
);

-- 2. Write a SQL statement that adds a few students to your table.
insert into students (name) values
    ('Dave M'),
    ('Bob T');

-- 3. Write a SQL statement that deletes a student from your table.
delete
from students
where name = 'Bob T';

-- 4. Write a SQL statement that updates a student record.
update students
set updated_at = now(), name = 'DLM'
where name = 'Dave M';

-- 5.Write a SQL statement that create a table classes and add a few classes.
-- You can decide on what information is stored about a class.
create table classes (
    id serial primary key,
    name text NOT NULL,
    starts_at timestamp NOT NULL,
    ends_at timestamp NOT NULL
);

insert into classes (name, starts_at, ends_at) values
    ('Intro calc', '2017-09-01 11:00', '2017-09-01 12:00'),
    ('Intro physics', '2017-09-01 12:00', '2017-09-01 13:00');

-- 6. Write a SQL statement that creates a table enrollment for students
-- enrolled in a class. An enrollment should always be valid. That is, we should
-- never have an enrollment for a non-existent student or class. Use a serial
-- primary key.
create table enrollments (
    id serial primary key,
    student_id int not null,
    class_id int not null,
    foreign key (student_id) references students(id),
    foreign key (class_id) references classes(id)
);

insert into enrollments (student_id, class_id) values
    (
        (select id from students where name = 'DLM'),
        (select id from classes where name = 'Intro calc')
    );

select enrollments.id, students.name, classes.name, classes.starts_at, classes.ends_at
from enrollments, students, classes
where enrollments.student_id = students.id and enrollments.class_id = classes.id
