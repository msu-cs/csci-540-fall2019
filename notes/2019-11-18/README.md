
# DB Security Setup

## Setup Node.js
Install node.js (this should also install npm)
* Ubuntu
    * sudo apt-get install nodejs
 * Mac and Windows
     * see [install docs](https://nodejs.org/en/download/).

## Download application
Download and unzip db_pres.tgz from discussion board

    tar xvzf db_pres.tgz

Install necessary modules (should all be contained in /node_modules and package.json)
    npm install
If this does not install necessary modules, run the following

    npm install express
    npm install body-parser
    npm install express-handlebars

## Database setup
Start postgres docker container

    docker run --net=host -p 5432:5432 adv-db/postgres

Start client

    docker run --net=host -it adv-db/clients psql -h localhost -U postgres postgres
    CREATE DATABASE demodb;

Load world.sql

    ~/csci-540-fall2019/env$ docker run --net=host -v ~/csci-540-fall2019/env/postgres:/mnt/ -it adv-db/clients bash
    psql -h localhost -U postgres -d demodb -f /mnt/world.sql

    *Note: this assumes you have world.sql at ~/csci-540-fall2019/env/postgres/

## Run Script from db_pres directory

    node app.js

## Open browser and go to localhost:3000
If everything worked correctly, you should see a rendered page.


