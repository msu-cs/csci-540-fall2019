#!/usr/bin/env python

import sys

sys.path.append("../../../hw/hw7/")

import mapper_reducer


def reducefunc(emit, key, vals):
    acc = 0
    for val in vals:
        weight = float(val[0])
        score = float(val[1])
        acc = acc + weight*score
    emit(key, acc)

mr = mapper_reducer.MapperReducer()
mr.reduce(reducefunc, sys.stdin)
