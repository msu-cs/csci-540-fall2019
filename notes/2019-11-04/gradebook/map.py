#!/usr/bin/env python

import sys
import re

sys.path.append("../../../hw/hw7/")

import mapper_reducer


def mapfunc(emit, handle):
    for line in handle:
        [aid, weight, score] = line.split()
        emit(aid, [weight, score])

mr = mapper_reducer.MapperReducer()
mr.map(mapfunc, './data')
