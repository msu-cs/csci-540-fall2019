#!/usr/bin/env python

import sys
import re

sys.path.append("../../../hw/hw7/")

import mapper_reducer


def mapfunc(emit, handle):
    for line in handle:
        for w0 in line.split():
            w = re.sub('[^a-z]', '', w0.lower())
            if w != '':
                emit(w, 1)

mr = mapper_reducer.MapperReducer()
mr.map(mapfunc, '.',
    omit=['.map.py.swp', '.reduce.py.swp']
)
