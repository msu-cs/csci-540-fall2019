
# RethinkDB
This is the official site of RethinkDB (https://rethinkdb.com/docs). In this link, you will find different RethinkDB documentations.

# Setting up RethinkDB your environment

For Mac User: (https://rethinkdb.com/docs/install/osx/)

    brew update && brew install rethinkdb

For Ubuntu user: (https://rethinkdb.com/docs/install/ubuntu/)

    source /etc/lsb-release && echo "deb https://download.rethinkdb.com/apt         $DISTRIB_CODENAME main" | sudo tee /etc/apt/sources.list.d/rethinkdb.list
    wget -qO- https://download.rethinkdb.com/apt/pubkey.gpg | sudo apt-key add -
    sudo apt-get update
    sudo apt-get install rethinkdb

For Windows user: (https://rethinkdb.com/docs/install/windows/)



## Setting up the server

In a terminal, to run the server:

    rethinkdb

## Setting up RethinkDB for Python

    pip install rethinkdb

## Test RethinkDB:

Initially run the server using 'rethinkdb' command and open http://localhost:8080/ and check RethinkDB table. Initially there will be no table. Then in a python file, just use these python code: (For example: rethink.py)

    from rethinkdb import RethinkDB
    r = RethinkDB()
    r.connect( "localhost", 28015).repl() #default port
    r.db('test').table_create('tv_shows').run()


Then, run python command:

    python rethink.py

Then open http://localhost:8080/ again, check RethinkDB table. There is a table named 'tv_shows' under 'test' database


*** if you face difficulties with python3, then you can try this

    https://rethinkdb.com/blog/chad-lung-python3/

# Reading

If you want learn more about Realtime Database, you can read these:

    https://users.soe.ucsc.edu/~sbrandt/courses/Winter00/290S/rtdb.pdf
